<?php
namespace Ekf\Exchange\Repository;

use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Iblock\PropertyTable;
use InvalidArgumentException;

class PropertyRepository
{
    /**
     * @param $iblockId
     * @return array
     */
    public function getProperties($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf('Ид инфоблока должен быть числом: "%s"', $iblockId));
        }

        $propsIterator = PropertyTable::getList([
            'filter' => [
                'IBLOCK_ID' => $iblockId,
                'ACTIVE' => 'Y'

            ]
        ]);

        $properties = [];

        while ($property = $propsIterator->fetch()) {
            $properties[] = $property;
        }

        return $properties;
    }

    /**
     * Получает варианты для свойства типа список
     *
     * @param $propertyId
     * @return array
     */
    public function getEnumValues($propertyId)
    {
        if ((int)$propertyId == 0) {
            throw new InvalidArgumentException(sprintf('Ид свойства должен быть числом: "%s"', $propertyId));
        }

        $propsIterator = PropertyEnumerationTable::getList([
            'filter' => [
                'PROPERTY_ID' => $propertyId

            ]
        ]);

        $values = [];

        while ($value = $propsIterator->fetch()) {
            $values[] = $value;
        }

        return $values;
    }

    /**
     * Создает вариант значения списка
     * @param $propertyId
     * @param $textValue
     * @return int
     * @throws \Exception
     */
    public function addEnumValue($propertyId, $textValue)
    {
        $result = PropertyEnumerationTable::add([
            'PROPERTY_ID' => $propertyId,
            'VALUE' => $textValue,
            'XML_ID' => md5($propertyId . '|' . $textValue)
        ]);

        if (!$result->isSuccess()) {
            throw new \Exception(sprintf(
                'Ошибка добавления варианта значения для свойства %d. Значение - %s. %s',
                $propertyId, $textValue, implode(', ', $result->getErrorMessages())
            ));
        }

        return $result->getId();
    }

    /**
     * @param array $propertyData
     * @throws \Exception
     * @return int
     */
    public function addProperty(array $propertyData)
    {
        if (empty($propertyData['CODE'])) {
            throw new InvalidArgumentException('Поле CODE обязательно');
        }

        $existingProperty = PropertyTable::getRow([
            'filter' => [
                'IBLOCK_ID' => $propertyData['IBLOCK_ID'],
                'CODE' => $propertyData['CODE']
            ]
        ]);

        if ($existingProperty) {
            throw new \Exception(sprintf(
                'Свойство с кодом %s уже существует: #%d', $propertyData['CODE'], $existingProperty['ID']
            ));
        }

        $addResult = PropertyTable::add($propertyData);

        if (!$addResult->isSuccess()) {
            throw new \Exception(sprintf(
                'Ошибка добавления свойства %s: %s', $propertyData['CODE'], implode(', ', $addResult->getErrorMessages())
            ));
        }

        return $addResult->getId();
    }

    /**
     * @param $propertyCode
     * @throws \Exception
     */
    public function deleteProperty($propertyCode)
    {
        if (empty($propertyCode)) {
            throw new InvalidArgumentException('Поле CODE обязательно');
        }

        $existingProperty = PropertyTable::getRow([
            'filter' => [
                'CODE' => $propertyCode
            ]
        ]);

        if ($existingProperty) {
            $result = PropertyTable::delete($existingProperty['ID']);

            if (!$result->isSuccess()) {
                throw new \Exception(sprintf(
                    'Ошибка удаления свойства %s', $propertyCode, implode(', ', $result->getErrorMessages())
                ));
            }
        }
    }
}