<?php
/**
 * Вспомогательные функции для работы с каталогом
 */
namespace Ekf\Exchange\Functions\Catalog;

use Bitrix\Catalog\ProductTable;

/**
 * Добавляет элемент каталога (в терминологии битрикс CCatalogProduct)
 *
 * @param $catalogProductData
 * @return array|int
 * @throws \Exception
 */
function addCatalogProduct($catalogProductData)
{
    $result = ProductTable::add($catalogProductData);

    if (!$result) {
        throw new \Exception(sprintf(
            'Не удалось добавить элемент каталога: %s', implode(', ', $result->getErrorMessages())
        ));
    }

    return $result->getId();
}

/**
 * Обновляет элемент каталога (в терминологии битрикс CCatalogProduct)
 *
 * @param $catalogProductData
 * @return array|int
 * @throws \Exception
 */
function updateCatalogProduct($catalogProductData)
{
    $result = ProductTable::update($catalogProductData['ID'], $catalogProductData);

    if (!$result->isSuccess()) {
        throw new \Exception(sprintf(
            'Не удалось обновить элемент каталога: %s', implode(', ', $result->getErrorMessages())
        ));
    }

    return $result->getId();
}

/**
 * Получает данные об элементе каталога (в терминологии битрикс CCatalogProduct) из данных о товаре из АПИ
 *
 * @param int $productId
 * @param array $apiProduct
 * @return array
 */
function getCatalogProductDataFromApiProduct(int $productId, array $apiProduct)
{
   $catalogProduct = [
       'ID' => $productId,
       'WEIGHT' => $apiProduct['unitStorage']['weight'] * 1000,
       'WIDTH' => $apiProduct['unitStorage']['width'] * 1000,
       'HEIGHT' => $apiProduct['unitStorage']['height'] * 1000,
       'LENGTH' => $apiProduct['unitStorage']['depth'] * 1000,
   ];

   return $catalogProduct;
}



