<?php
namespace Ekf\Exchange\Functions\Util;

function getProductCode($productName)
{
    $code = \CUtil::translit($productName, 'ru', [
        'replace_space' => '-',
        'replace_other' => '-',
    ]);

    if (empty($code)) {
        throw new \Exception(sprintf(
            'Не удалось получить код для товара %s', $productName
        ));
    }

    return $code;
}