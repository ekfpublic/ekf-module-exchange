<?php
/**
 * Вспомогательные функции для работы с событиями
 */
namespace Ekf\Exchange\Functions\Events;

use Bitrix\Main\Event;
use Ekf\Exchange\Entity\ProductEntity;

/**
 * Событие вызывается после получения данных товара из АПИ
 * Используется для изменения "сырых" данных, полученных из АПИ
 * @param array $productData Данные о товаре

 * Пример использования:
 *
 * $eventManager = \Bitrix\Main\EventManager::getInstance();
 * $eventManager->addEventHandler('ekf.exchange', 'before_product_data_parsed', function(\Bitrix\Main\Event $e){
 *      $productData = $e->getParameter('productData');
 *      $productData['name'] = 'New Name';
 *      return new Bitrix\Main\EventResult($e->getEventType(), array('productData' => $productData));
 * });
 *
 * @return array
 */
function productBeforeDataParsed($productData) : array
{
    $ev = new Event('ekf.exchange', 'before_product_data_parsed', [
        'productData' => $productData
    ]);

    $ev->send();

    $resultData = [];

    foreach ($ev->getResults() as $eventResult) {
        $params = $eventResult->getParameters();

        if (null != ($modifiedProductData = $params['productData'])) {
            $resultData = $modifiedProductData;
        }
    }

    return $resultData;
}

/**
 * Событие вызывается после создания сущности товара, перед сохранением этой сущности в БД
 * Позволяет изменить данные товара перед сохранением
 *
 * @param ProductEntity $product Товар
 * @param array $productData Данные товара из АПИ
 * @return ProductEntity
 */
function productAfterEntityCreated(ProductEntity $product, array $productData) : ProductEntity
{
    $ev = new Event('ekf.exchange', 'after_product_entity_created', [
        'productEntity' => $product,
        'productApiData' => $productData,
    ]);

    $ev->send();

    foreach ($ev->getResults() as $eventResult) {
        $modifiedProductData = $eventResult->getParameters();
        if (null !== $modifiedProductData) {
            return $modifiedProductData;
        }
    }

    return $product;
}


/**
 * Событие вызывается после получения данных раздела из АПИ
 * Используется для изменения "сырых" данных, полученных из АПИ
 *
 * Пример использования см. в комментариях к productBeforeDataParsed
 *
 * @param $categoryData
 * @return array
 */
function categoryBeforeDataParsed($categoryData) : array
{
    $ev = new Event('ekf.exchange', 'before_category_data_parsed', [
        'categoryData' => $categoryData
    ]);

    $ev->send();

    $resultData = [];

    foreach ($ev->getResults() as $eventResult) {
        $params = $eventResult->getParameters();

        if (null != ($modifiedProductData = $params['categoryData'])) {
            $resultData = $modifiedProductData;
        }
    }

    return $resultData;
}
