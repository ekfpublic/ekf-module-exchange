<?php
/**
 * Вспомогательные функции для обмена разделами
 */
namespace Ekf\Exchange\Functions\Categories;

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\DB\Exception;
use Ekf\Exchange\Service\ApiClient;

/**
 * @param $apiClient ApiClient
 */
function getSortedApiSection($apiClient)
{
    $apiSections = $apiClient->getCategories();

    // Отсортируем в порядке возрастания вложенности
    usort($apiSections, function($a, $b){
        if ($a['hierarchyLevel'] == $b['hierarchyLevel']) {
            return 0;
        }
        return ($a['hierarchyLevel'] < $b['hierarchyLevel']) ? -1 : 1;
    });

    return $apiSections;
}

function getSectionCode($sectionName)
{
    $sectionNamePrepared = preg_replace('/^[\d.\s]+/', '', $sectionName);

    $sectionNamePrepared = trim($sectionNamePrepared);

    $code = \CUtil::translit($sectionNamePrepared, 'ru', [
        'replace_space' => '-',
        'replace_other' => '-',
    ]);

    if (empty($code)) {
        throw new \Exception(sprintf(
            'Не удалось получить код для раздела %s', $sectionName
        ));
    }

    return $code;
}

function getSectionByXmlId($iblockId, $sectionXmlId)
{
    $section = \CIBlockSection::GetList(
        [],
        [
            'IBLOCK_ID' => $iblockId,
            'XML_ID' => $sectionXmlId,
            'CHECK_PERMISSIONS' => 'N'
        ],
        false,
        [
            'ID',
            'XML_ID',
            'IBLOCK_SECTION_ID',
            'UF_IS_EKF',
            'UF_EKF_HASH',
            'DEPTH_LEVEL',
            'PICTURE'
        ]
    )->Fetch();

    return $section;
}

function createSection($iblockId, $parentId, $name, $xmlId, $code, $imagePath = '')
{
    $sectionsFields = [
        'IBLOCK_ID' => $iblockId,
        'IBLOCK_SECTION_ID' => $parentId,
        'NAME' => $name,
        'CODE' => $code,
        'XML_ID' => $xmlId,
        'UF_IS_EKF' => 1,
        'skip_ekf_hash_reset' => true,
    ];

    if (!empty($imagePath)) {
        $img = \CFile::MakeFileArray($imagePath);
        if ($img && $img['type'] != 'application/x-rar') {
            $sectionsFields['PICTURE'] = $img;
        }
    }

    $repository = new \CIBlockSection();

    $sectionId = $repository->Add($sectionsFields);

    if (!$sectionId) {
        throw new Exception(sprintf(
            'Не удалось создать раздел: %s %s', $repository->LAST_ERROR, print_r($sectionsFields, true)
        ));
    }

    return $sectionId;
}

function updateSection($sectionId, $sectionsFields)
{
    $sectionsFields['skip_ekf_hash_reset'] = true;
    $sectionsFields['UF_IS_EKF'] = 1;

    if (!empty($sectionsFields['+IMG'])) {
        $img = \CFile::MakeFileArray($sectionsFields['+IMG']);
        if ($img && $img['type'] != 'application/x-rar') {
            $sectionsFields['PICTURE'] = $img;
        }
    }

    $repository = new \CIBlockSection();

    $bOk = $repository->Update($sectionId, $sectionsFields);

    if (!$bOk) {
        throw new Exception(sprintf(
            'Не удалось обновить раздел %d: %s %s', $sectionId, $repository->LAST_ERROR, print_r($sectionsFields, true)
        ));
    }

    return $bOk;
}

function getFullSiteSectionsList($iblockId)
{
    $iterator = \CIBlockSection::GetList(
        [],
        [
            'IBLOCK_ID' => $iblockId,
            'ACTIVE' => 'Y',
            'CHECK_PERMISSIONS' => 'N'
        ],
        false,
        [
            'ID',
            'XML_ID',
            'IBLOCK_SECTION_ID',
            'UF_IS_EKF',
            'UF_EKF_HASH',
            'PICTURE'
        ]
    );

    $sections = [];

    while ($section = $iterator->Fetch()) {
        $sections[$section['ID']] = [
            'UPDATED' => false,
            'IS_EKF' => $section['UF_IS_EKF'] == 1,
            'IBLOCK_SECTION_ID' => $section['IBLOCK_SECTION_ID'],
            'PICTURE' => $section['PICTURE'],
        ];
    }

    return $sections;
}

/**
 * Устанавливает признак того, что раздел обновлен
 * При этом признак устанавливается не только у самого раздела, но и у всех родителей
 * Нужно для deactivateNonExistingSections
 *
 * @param $siteSectionsList
 * @param $sectionId
 */
function setSectionUpdated(&$siteSectionsList, $sectionId)
{
    do {
        if (!array_key_exists($sectionId, $siteSectionsList)) {
            break;
        }

        $siteSectionsList[$sectionId]['UPDATED'] = true;

        $sectionId = $siteSectionsList[$sectionId]['IBLOCK_SECTION_ID'];
    } while(true);
}

/**
 * Отключает разделы на сайте, которых нет в АПИ
 * Изначально получаем все разделы сайта через getFullSiteSectionsList и отмечаем те, что получили из АПИ
 * Затем вызовом этого метода деактивируем несуществующие разделы
 * @param $siteSectionsList
 */
function deactivateNonExistingSections($siteSectionsList)
{
    $repository = new \CIBlockSection();

    foreach ($siteSectionsList as $id => $info) {
        if ($info['IS_EKF'] && !$info['UPDATED']) {
            $repository->Update($id, ['ACTIVE' => 'N', 'UF_EKF_HASH' => '']);
        }
    }
}