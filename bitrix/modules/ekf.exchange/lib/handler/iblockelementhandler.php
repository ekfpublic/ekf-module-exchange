<?php
namespace Ekf\Exchange\Handler;

use Bitrix\Main\Loader;
use Ekf\Exchange\Service\Config;

class IblockElementHandler
{
    /**
     * Список полей основной таблицы элементов инфоблока
     * Если изменяют только их, нам нужно сбросить только свойство EKF_HASH, т.к. свойства и файлы не изменились
     * @var array
     */
    static $mainTableFields = array (
        'WF',
        'SEARCHABLE_CONTENT',
        'IBLOCK_SECTION',
        'ID',
        'IBLOCK_ID',
        'RESULT'
    );

    /**
     * При изменении товара сбросим хэши, чтобы при обмене данные товара обновились
     * @param $arFields
     */
    public static function resetHashesOnElementChange($arFields)
    {
        static $iblockId = -1;

        if ($iblockId == -1) {
            if (Loader::includeModule('ekf.exchange')) {
                $iblockId = (int)(new Config())->getIblockId();
            }
        }

        if ($arFields['skip_ekf_hash_reset'] === true) {
            return;
        }

        if ((int)$arFields['ID'] > 0 && $arFields['IBLOCK_ID'] == $iblockId) {
            if (count(array_diff(array_keys($arFields), self::$mainTableFields)) == 0) {
                $updateData = [
                    'EKF_HASH' => ''
                ];
            } else {
                $updateData = [
                    'EKF_HASH' => '',
                    'EKF_HASH_P' => '',
                    'EKF_HASH_F' => '',
                    'EKF_HASH_D' => '',
                ];
            }

            \CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], $updateData);
        }
    }
}