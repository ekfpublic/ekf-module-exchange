<?php
namespace Ekf\Exchange\Factory;

use Ekf\Exchange\Entity\ProductEntity;
use Ekf\Exchange\Exception\BadProductDataException;
use Ekf\Exchange\Exception\BadPropertyDataException;

/**
 * Фабрика для объектов ProductEntity
 * Служит для создания товара по данным из АПИ
 * Есть небольшое расхождение в терминах: В АПИ часть данных является полями товара, а в битрикс - свойствами
 * Например артикул, серия... Поэтому есть два класса ProductFactory и ProductPropertyFactory
 * Первый создает товар из "Товара" в понимании АПИ, второй - товар из свойств
 */
class ProductFactory
{
    /** @var PropertyValueFactory  */
    private $propertyValueFactory;

    public function __construct()
    {
        $this->propertyValueFactory = new PropertyValueFactory();
    }

    /**
     * @param $productData
     * @return ProductEntity
     * @throws BadProductDataException
     */
    public function buildFromArray($productData)
    {
        if (!is_array($productData) || empty($productData)) {
            throw new \InvalidArgumentException('Данные о товаре должны быть непустым массивом');
        }

        $productData['category_id'] = $productData['productGroup']['id'];

        $productEntity = new ProductEntity();

        $productEntity->setActive(true);

        $productEntity->setXmlId(
            $this->getField($productData, 'id')
        );

        $productEntity->setSectionId(
            $this->getField($productData, 'category_id')
        );

        $productEntity->setDetailText($productData['description'] ?? '');

        $productName = $this->getField($productData, 'shortName');

        $productEntity->setName($productName);
        $productEntity->setCode(\CUtil::translit($productName, 'ru'));

        $productEntity->setVendorCode($this->getField($productData, 'vendorCode'));

        foreach (['vendorCode', 'status', 'multiplicity', 'name', 'bazoved', 'volume'] as $code) {
            try {
                $propertyEntity = $this->propertyValueFactory->buildFromArray([
                    'api_code' => $code == 'name' ? 'nameFull' : $code,
                    'value'    => $this->getField($productData, $code)
                ]);

                $productEntity->addToPropertyCollection($propertyEntity);
            } catch (BadPropertyDataException $ex) {

            }
        }

        // Дополнительные устройства
        if( array_key_exists('relatedProducts',$productData) ){
            $accessories = array_column($productData['relatedProducts'], 'id');
            $accessories = (is_array($accessories)) ? $accessories : [];
        }else{
            $accessories = [];
        }

        try {
            $propertyEntity = $this->propertyValueFactory->buildFromArray([
                'api_code' => 'dopustr',
                'value'    => $accessories
            ]);

            $productEntity->addToPropertyCollection($propertyEntity);

            $propertyEntity = $this->propertyValueFactory->buildFromArray([
                'api_code' => 'series',
                'value'    => $this->getField($productData, $code, false)
            ]);

            $productEntity->addToPropertyCollection($propertyEntity);
        } catch (BadPropertyDataException $ex) {

        }

        // Запишем признак того, что товар EKF
        try {
            $propertyEntity = $this->propertyValueFactory->buildFromArray([
                'code' => 'EKF_IS_EKF',
                'value'    => 1
            ]);

            $productEntity->addToPropertyCollection($propertyEntity);
        } catch (BadPropertyDataException $ex) {

        }


        return $productEntity;
    }

    /**
     * @param array $productData
     * @param $code
     * @param bool $required
     * @return string
     * @throws BadProductDataException
     */
    private function getField(array $productData, $code, $required = true)
    {
        $value = trim($productData[$code]);

        if ($required && empty($value)) {
            throw new BadProductDataException(sprintf(
                'В данных товара %s отсутствует либо пусто обязательное поле %s', $productData['id'], $code
            ));
        }

        return $value;
    }
}