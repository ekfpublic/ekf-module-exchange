<?php
namespace Ekf\Exchange\Factory;

use Ekf\Exchange\Entity\FileEntity;
use InvalidArgumentException;

/**
 * Фабрика для объектов FileEntity
 */
class FileValueFactory
{
    /**
     * @param $valueData array
     * @return FileEntity
     * @throws InvalidArgumentException
     */
    public function buildFromArray($valueData)
    {
        if (!is_array($valueData) || empty($valueData)) {
            throw new InvalidArgumentException('Данные о файле должны быть непустым массивом');
        }

        if (!array_key_exists('value', $valueData)) {
            throw new InvalidArgumentException('Для значения свойства должно быть задано поле value');
        }

        $code = trim($valueData['api_code']);

        $id = (int)$valueData['id'];

        if (empty($code) && $id <= 0) {
            throw new InvalidArgumentException('Для значения свойства должен быть заполнен либо код, либо ИД');
        }

        $valueEntity = new FileEntity();

        if (!empty($code)) {
            $valueEntity->setApiCode($code);
        }

        if ($id > 0) {
            $valueEntity->setPropertyId($id);
        }

        if (!empty($valueData['name'])) {
            $valueEntity->setName($valueData['name']);
        }

        if (!empty($valueData['unit'])) {
            $valueEntity->setUnit($valueData['unit']);
        }

        if (!empty($valueData['etim_unit'])) {
            $valueEntity->setEtimUnit($valueData['etim_unit']);
        }

        $valueEntity->setValue($valueData['value']);

        return $valueEntity;
    }
}