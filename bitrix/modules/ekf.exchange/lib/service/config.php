<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 * Date: 12.07.2018
 */

namespace Ekf\Exchange\Service;

use Bitrix\Main\Config\Option;
use InvalidArgumentException;

/**
 * Получение/сохранение настроек
 */
class Config
{
    const MODULE_ID = 'ekf.exchange';

    const IDENTIFY_BY_XML_ID = 'XML_ID';
    const IDENTIFY_BY_VENDOR_CODE = 'VENDOR_CODE';

    /**
     * @return mixed
     */
    public function getIblockId()
    {
        return (int)Option::get(self::MODULE_ID, 'products_iblock', 0);
    }

    /**
     * @param mixed $iblockId
     */
    public function saveIblockId($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException('Ид инфоблока должен быть числом');
        }

        Option::set(self::MODULE_ID, 'products_iblock', $iblockId);
    }

    /**
     * Получает ключ (токен) доступа к АПИ
     * @return mixed
     */
    public function getApiKey()
    {
        return Option::get(self::MODULE_ID, 'api_key', '');
    }

    /**
     * @param mixed $value
     */
    public function saveApiKey($value)
    {
        $value = trim($value);

        if (strlen($value) == 0) {
            throw new InvalidArgumentException('Ключ апи не может быть пустым');
        }

        Option::set(self::MODULE_ID, 'api_key', $value);
    }

    /**
     * Получает код свойства для загрузки доп. изображений
     * @return mixed
     */
    public function getMorePhotoPropertyCode()
    {
        static $code = '';

        if (empty($code)) {
            $code = Option::get(self::MODULE_ID, 'more_photo_code', '');
        }

        return $code;
    }

    /**
     * @param mixed $value
     */
    public function saveMorePhotoPropertyCode($value)
    {
        $value = trim($value);

        if (strlen($value) == 0) {
            throw new InvalidArgumentException('Код свойства для загрузки дополнительных изображений не может быть пустым');
        }

        Option::set(self::MODULE_ID, 'more_photo_code', $value);
    }

    /**
     * Получает карту соответствия свойств АПИ и сайта
     * @return mixed
     */
    public function getPropertiesMap()
    {
        $mapFull = [];

        for ($i = 0; $i < 500; $i++) {
            $serializedMap = Option::get(self::MODULE_ID, 'properties_map_' . $i, '');

            $map = unserialize($serializedMap);

            if (!is_array($map)) {
                break;
            }

            $mapFull = array_merge($mapFull, $map);
        }

        return $mapFull;
    }

    /**
     * @param mixed $value
     */
    public function savePropertiesMap($value)
    {
        $map = [];

        foreach ($value as $apiCode => $siteCode) {
            $siteCode = trim($siteCode);

            if (!empty($siteCode)) {
                $map[$apiCode] = $siteCode;
            }
        }

        // Слишком много свойств, сохраним пачками
        $mapChunks = array_chunk($map, 10, true);

        $i = 0;

        $connection = \Bitrix\Main\Application::getConnection();
        $connection->startTransaction();

        foreach ($mapChunks as $i => $mapChunk) {
            Option::set(self::MODULE_ID, 'properties_map_' . $i, serialize($mapChunk));
        }

        while ($i++ < 500) {
            Option::delete(self::MODULE_ID, [
                'name' => 'properties_map_' . $i
            ]);
        }

        $connection->commitTransaction();
    }

    /**
     * Получает карту соответствия цен АПИ и сайта
     * @return mixed
     */
    public function getPricesMap()
    {
        $serializedMap = Option::get(self::MODULE_ID, 'prices_map', '');

        $map = unserialize($serializedMap);

        if (!is_array($map)) {
            $map = [];
        }

        return $map;
    }

    /**
     * @param mixed $value
     */
    public function savePricesMap($value)
    {
        $map = [];

        foreach ($value as $apiId => $siteId) {
            $siteId = (int)$siteId;

            if ($siteId > 0) {
                $map[$apiId] = $siteId;
            }
        }
        Option::set(self::MODULE_ID, 'prices_map', serialize($map));
    }

    /**
     * Получает карту соответствия цен апи и размера скидок
     */
    public function getPriceDiscountsMap()
    {
        $serializedMap = Option::get(self::MODULE_ID, 'prices_discount_map', '');

        $map = unserialize($serializedMap);

        if (!is_array($map)) {
            $map = [];
        }

        return $map;
    }

    public function savePriceDiscountsMap($value)
    {
        $map = [];

        foreach ($value as $apiId => $discount) {
            $discount = (int)$discount;

            if ($discount != 0) {
                $map[$apiId] = $discount;
            }
        }

        Option::set(self::MODULE_ID, 'prices_discount_map', serialize($map));
    }


    /**
     * Получает соответствие складов АПИ и сайта
     * @return array|mixed
     */
    public function getWarehousesMap()
    {
        $serializedMap = Option::get(self::MODULE_ID, 'warehouses_map', '');

        $map = unserialize($serializedMap);

        if (!is_array($map)) {
            $map = [];
        }

        return $map;
    }

    public function saveWarehousesMap($value)
    {
        $map = [];

        foreach ($value as $apiId => $siteId) {
            if (!empty($siteId)) {
                $map[$apiId] = $siteId;
            }
        }

        Option::set(self::MODULE_ID, 'warehouses_map', serialize($map));
    }

    /**
     * Получает соответствие разделов АПИ и сайта
     * @return array|mixed
     */
    public function getSectionsMap()
    {
        $serializedMap = Option::get(self::MODULE_ID, 'sections_map', '');

        $map = unserialize($serializedMap);

        if (!is_array($map)) {
            $map = [];
        }

        return $map;
    }

    public function saveSectionsMap($value)
    {
        $map = [];

        foreach ($value['match'] as $siteId => $apiIds) {
            $apiIds = array_filter($apiIds);
            if (!empty($apiIds)) {
                foreach ($apiIds as $apiId) {
                    /** @var bool $collapse Схлопывать разделы */
                    $collapse = $value['collapse'][$siteId][$apiId] == 'y';
                    $map[$siteId][] = [$apiId, $collapse];
                }

            }
        }

        Option::set(self::MODULE_ID, 'sections_map', serialize($map));
    }

    /**
     * Получает код свойства для загрузки файлов документации
     * @return mixed
     */
    public function getDocsPropertyCode()
    {
        static $code = '';

        if (empty($code)) {
            $code = Option::get(self::MODULE_ID, 'docs_files_code', '');
        }

        return $code;
    }

    /**
     * @param mixed $value
     */
    public function saveDocsPropertyCode($value)
    {
        $value = trim($value);
        Option::set(self::MODULE_ID, 'docs_files_code', $value);
    }

    /**
     * Получает способ сопоставления товаров АПИ и на сайте (по артикулу, внешнему коду)
     * @return mixed
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public function getProductIdentifyMethod()
    {
        static $code = '';

        if (empty($code)) {
            $code = Option::get(self::MODULE_ID, 'product_identify_method', '');
        }

        return $code;
    }

    /**
     * @param mixed $value
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public function saveProductIdentifyMethod($value)
    {
        $value = trim($value);
        Option::set(self::MODULE_ID, 'product_identify_method', $value);
    }
}