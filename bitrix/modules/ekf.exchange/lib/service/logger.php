<?php
namespace Ekf\Exchange\Service;

use Exception;

class Logger
{
    /** Макс. количество лог файлов каждого типа (10 для товаров, 10 для разделов и т.д.) */
    const MAX_LOG_FILES = 10;

    private $lineEnding;
    private $logDirectory;

    /** @var bool  */
    private $suppressOutput;

    private $logFileHandle = null;

    /**
     * Logger constructor.
     * @param $type (products, features.....)
     * @param bool $suppressOutput
     * @throws Exception
     */
    public function __construct($type, $suppressOutput = false)
    {
        if (PHP_SAPI == 'cli') {
            $this->lineEnding = PHP_EOL;
        } else {
            $this->lineEnding = '<br>';
        }

        $this->suppressOutput = $suppressOutput;

        /**
         * Создадим директорию для логов
         */
        $this->logDirectory = $_SERVER['DOCUMENT_ROOT'] . '/upload/ekf_exchange_logs/';
        if (!file_exists($this->logDirectory)) {
            $bOk = mkdir($this->logDirectory);
            if (!$bOk) {
                throw new Exception(sprintf(
                    'Не удалось создать директорию для хранения логов %s', $this->logDirectory
                ));
            }
        }

        /**
         * Откроем файл лога
         */
        $logFileName = sprintf('%s-%s.txt', $type, date('Y-m-d'));

        $this->logFileHandle = fopen($this->logDirectory . $logFileName, 'a');

        if (!$this->logFileHandle) {
            throw new Exception(sprintf(
                'Не удалось открыть файл для записи логов %s', $this->logDirectory . $logFileName
            ));
        }

        $this->deleteOldLogFiles($type);
    }

    public function formatException(Exception $ex, $additionalMessage = '')
    {
        $message = [];

        if (!empty($additionalMessage)) {
            $message[] = $additionalMessage;
        }

        $exceptionMessage = $ex->getMessage();
        if (!empty($exceptionMessage)) {
            $message[] = $exceptionMessage;
        }

        return implode(' / ', $message);
    }

    public function critical($message, $context = [])
    {
        $this->log('CRITICAL', $message, $context);
    }

    public function error($message, $context = [])
    {
        $this->log('ERROR', $message, $context);
    }

    public function info($message, $context = [])
    {
        $this->log('INFO', $message, $context);
    }

    // Отдельно выделим логи о состояниях обмена - начат обмен, закончен обмен...
    public function state($message, $context = [])
    {
        $this->log('STATE', $message, $context);
    }

    private function log($level, $message, $context = [])
    {
        $logRecord = sprintf('%s    [%s]    %s  %s', date('Y-m-d H:i:s'), $level, $message, json_encode($context));

        fwrite($this->logFileHandle, $logRecord . PHP_EOL);

        if (!$this->suppressOutput) {
            echo $message . $this->lineEnding;
            ob_end_flush();
        }
    }

    /**
     * Удаляет старые файлы логов
     * @param string $type
     * @throws Exception
     */
    private function deleteOldLogFiles(string $type)
    {
        $logFiles = glob($this->logDirectory . $type . '*');
        // Отсортируем, старые файлы в начале
        sort($logFiles);

        if (count($logFiles) > self::MAX_LOG_FILES) {
            $logFilesToDelete = array_slice($logFiles, 0, count($logFiles) - self::MAX_LOG_FILES);

            foreach ($logFilesToDelete as $file) {
                if (!unlink($file)) {
                    throw new \Exception(sprintf(
                        'Не удалось удалить файл логов %s', $file
                    ));
                }
            }
        }
    }
}