<?php
namespace Ekf\Exchange\Service;


use Ekf\Exchange\Exception\ApiEmptyResponseException;
use Ekf\Exchange\Repository\QueueRepository;

class DataDownloader
{
    /**
     * Загружает данные из АПИ во временную таблицу-очередь
     *
     * @param callable $apiRequestCallback Коллбек для запроса к АПИ
     * @param $limit
     * @param $type
     * @return int
     */
    public function loadDataToQueue(callable $apiRequestCallback, $limit, $type)
    {
        $numRecords = 0;

        $page = ($limit === false) ? false : 1;

        $queueRepository = new QueueRepository();

        //Нужно для теста, чтобы не запрашивать данные из АПИ каждый раз
        if (defined('ekf.exchange.dbcache')) {
            $queueRepository->resetUpdatedFlag();
            return $queueRepository->getQueueSize();
        }

        $queueRepository->clearQueue();

        do {
            try {
                $products = $apiRequestCallback($page, $limit);
            } catch (ApiEmptyResponseException $ex) {
                break;
            }

            $page++;

            $connection = \Bitrix\Main\Application::getConnection();
            $productsPacks = array_chunk($products, 500);
            foreach ($productsPacks as $productsPack) {

                $connection->startTransaction();

                try {
                    foreach ($productsPack as $product) {
                        $queueRepository->enqueue($product['id'], serialize($product), $type);

                        $numRecords++;
                    }

                    $connection->commitTransaction();

                } catch (\Exception $ex) {
                    $connection->rollbackTransaction();
                    throw $ex;
                }
            }



            // Загрузить все за раз
            if ($limit === false) {
                break;
            }

        } while(1);

        return $numRecords;
    }
}