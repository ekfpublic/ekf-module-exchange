<?php
namespace Ekf\Exchange\Service\PriceMap;

use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;

/**
 * Служит для настройки соответствия цен сайта и АПИ
 */

class PriceMapper
{
    /** @var Config  */
    private $configManager;

    /** @var ApiClient  */
    private $apiClient;

    public function __construct()
    {
        $this->configManager = new Config();
        $this->apiClient = new ApiClient($this->configManager->getApiKey());

        \Bitrix\Main\Loader::includeModule('catalog');
    }

    public function show()
    {
        $apiPrices = [
            ['id' => '6dcb3f5a-f670-11d8-b667-000a48086d14', 'name' => 'Базовая цена'],
            ['id' => 'c250e6a3-b86b-11e5-8e48-000c29c6d5f2', 'name' => 'РРЦ'],

        ];

        $sitePriceTypes = $this->getSitePriceTypes();

        $pricesMap = $this->configManager->getPricesMap();

        $disountsMap = $this->configManager->getPriceDiscountsMap();

        require(__DIR__ . '/templates/template.php');
    }

    private function getSitePriceTypes()
    {
        $priceIterator = \CCatalogGroup::GetList();

        $priceTypes = [];

        while ($price = $priceIterator->Fetch()) {
            $priceTypes[] = $price;
        }

        return $priceTypes;
    }
}