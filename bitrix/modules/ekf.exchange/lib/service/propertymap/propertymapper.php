<?php
namespace Ekf\Exchange\Service\PropertyMap;

use Bitrix\Iblock\PropertyTable;
use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;

/**
 * Служит для настройки соответствия свойств сайта и АПИ
 */

class PropertyMapper
{
    /** @var Config  */
    private $configManager;

    /** @var ApiClient  */
    private $apiClient;

    public function __construct()
    {
        $this->configManager = new Config();
        $this->apiClient = new ApiClient($this->configManager->getApiKey());
    }

    public function show()
    {
        $apiProperties = $this->apiClient->getProperties();

        usort($apiProperties, function($a, $b){
           if ($a['name'] == $b['name']) {
               return 0;
           }

           return ($a['name'] < $b['name']) ? -1 : 1;
        });

        $propertiesMap = $this->configManager->getPropertiesMap();

        require(__DIR__ . '/templates/template.php');
    }
}