<?php
namespace Ekf\Exchange\Service\SectionMap;

use Bitrix\Iblock\SectionTable;
use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;

/**
 * Служит для настройки соответствия разделов сайта и АПИ
 */

class SectionMapper
{
    /** @var Config  */
    private $configManager;

    /** @var ApiClient  */
    private $apiClient;

    public function __construct()
    {
        $this->configManager = new Config();
        $this->apiClient = new ApiClient($this->configManager->getApiKey());
    }

    public function show()
    {
        $apiSections = $this->apiClient->getCategories();

        $apiSectionsByParent = $this->prepareApiSections($apiSections);

        $siteSections = $this->getSiteSections();

        // Соответствие разделов сайта и АПИ. Только идентификаторы.
        // А нам нужны еще и названия. Получим их ниже.
        $sectionsMapRaw = $this->configManager->getSectionsMap();

        $apiSectionsById = [];
        foreach ($apiSections as $apiSection) {
            $apiSectionsById[$apiSection['id']] = $apiSection['name'];
        }

        $sectionsMap = [];

        foreach ($sectionsMapRaw as $siteId => $confItems) {

            foreach ($confItems as list($apiId, $collapse)) {
                if (array_key_exists($apiId, $apiSectionsById)) {
                    $sectionsMap[$siteId][] = [
                        'id'   => $apiId,
                        'collapse' => $collapse,
                        'name' => $apiSectionsById[$apiId]
                    ];
                }
            }

        }

        require(__DIR__ . '/templates/template.php');
    }

    private function getSiteSections()
    {
        $iterator = SectionTable::getList([
            'filter' => [
                'IBLOCK_ID' => (int)$this->configManager->getIblockId(),
                'ACTIVE' => 'Y',
                'GLOBAL_ACTIVE' => 'Y',
            ],
            'select' => [
                'ID',
                'NAME',
                'DEPTH_LEVEL',
                'IBLOCK_SECTION_ID'
            ],
            'order' => [
                'LEFT_MARGIN' => 'ASC',
                'NAME' => 'ASC'
            ]
        ]);

        $sections = [];

        while ($section = $iterator->fetch()) {
            $sections[$section['ID']] = $section;
        }

        return $sections;
    }

    private function prepareApiSections($apiSections)
    {
        // У всех корневых разделов parentId=33048d6b-a7a0-4d22-9d84-4e712686ae2e
        // Но у некоторых по непонятным причинам не так, исправим это
        foreach ($apiSections as &$apiSection) {

            /* старая иерархия- при наличи верхнего уровня
            if ($apiSection['parentId'] == '00000000-0000-0000-0000-000000000000') {
                $apiSection['parentId'] = '33048d6b-a7a0-4d22-9d84-4e712686ae2e';
                $apiSection['hierarchyLevel'] = 1;
            } */
            if ($apiSection['parentId'] == '33048d6b-a7a0-4d22-9d84-4e712686ae2e') {
                $apiSection['hierarchyLevel'] = 1;
            }
        }

        unset($apiSection);

        usort($apiSections, function($a, $b){
            if ($a['hierarchyLevel'] == $b['hierarchyLevel']) {
                if ($a['name'] == $b['name']) {
                    return 0;
                }

                return ($a['name'] < $b['name']) ? -1 : 1;
            }

            return ($a['hierarchyLevel'] < $b['hierarchyLevel']) ? -1 : 1;
        });

        $apiSectionsByParent = [];

        foreach ($apiSections as $apiSection) {
            $apiSectionsByParent[$apiSection['parentId']][] = $apiSection;
        }

        return $apiSectionsByParent;
    }
}