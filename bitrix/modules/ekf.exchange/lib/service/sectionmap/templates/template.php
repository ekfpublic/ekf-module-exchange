<?
/**
 * @var array $siteSections
 * @var array $apiSections
 * @var array $sectionsMap
 */

CUtil::InitJSCore(['jquery']);
?>

<style><?= file_get_contents(__DIR__ . '/style.css') ?></style>

<script><?= file_get_contents(__DIR__ . '/js/match.js') ?></script>

<script><?= file_get_contents(__DIR__ . '/js/match_collection.js') ?></script>

<script><?= file_get_contents(__DIR__ . '/script.js') ?></script>

<div class="section-mapper">
    <p>Выделите раздел в дереве сайта</p>
    <p>Выделите раздел в дереве АПИ</p>
    <p>Нажмите на стрелочку. Выделенный раздел АПИ будет выгружаться в выделенный раздел сайта.</p>
    <p>Выделите чекбокс, если необходимо схлопывание артикулов</p>

    <div class="section-mapper__sections" id="section-mapper-site">

        <p><b>Разделы сайта</b> <a href="#" class="js-section-mapper-toggle">Свернуть все</a></p>

        <? $previousSection = false; ?>

        <? foreach ($siteSections as $siteSection) { ?>

            <? if ($previousSection && $siteSection['DEPTH_LEVEL'] <= (int)$previousSection['DEPTH_LEVEL']) { ?>

                <div class="section-mapper__collapser">&nbsp;</div>
                <div class="section-mapper__section-name"><?= $previousSection['NAME'] ?> [<?= $previousSection['ID'] ?>]</div>
                <div class="section-mapper__matches"></div>
                <div class="section-mapper__clearfix"></div>
                <?= str_repeat('</div>', ((int)$previousSection['DEPTH_LEVEL'] - $siteSection['DEPTH_LEVEL']) + 1) ?>

            <? } else if ($previousSection) { ?>

                <div class="section-mapper__collapser">
                    <a class="section-mapper__section-collapse"></a>
                </div>
                <div class="section-mapper__section-name"><?= $previousSection['NAME'] ?> [<?= $previousSection['ID'] ?>]</div>
                <div class="section-mapper__matches"></div>
                <div class="section-mapper__clearfix"></div>

            <? }?>

            <? $previousSection = $siteSection; ?>

            <div
                    class="section-mapper__section section-depth-<?= $siteSection['DEPTH_LEVEL'] ?>"
                    data-match-info='<?= json_encode($sectionsMap[$siteSection['ID']]) ?>'
                    data-section-id="<?= $siteSection['ID'] ?>">

        <? } ?>

        <div class="section-mapper__collapser">&nbsp;</div>
        <div class="section-mapper__section-name"><?= $previousSection['NAME'] ?> [<?= $previousSection['ID'] ?>]</div>
        <div class="section-mapper__matches"></div>
        <div class="section-mapper__clearfix"></div>

        <?= str_repeat('</div>', ((int)$previousSection['DEPTH_LEVEL']) + 1) ?>

    </div>
    <div class="section-mapper__sections" id="section-mapper-api">

        <div class="section-mapper__sections-sticky">

        <p><b>Разделы из АПИ</b></p>

        <?php $section = ['id' => '33048d6b-a7a0-4d22-9d84-4e712686ae2e']; ?>

        <?php require (__DIR__ . '/inc/sections.php'); ?>

        </div>

    </div>
</div>