<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 * Date: 22.08.2018
 */

namespace Ekf\Exchange\Service;


use Bitrix\Catalog\ProductTable;
use Bitrix\Main\Loader;

class CatalogProductFinder
{
    private $iblockId;

    private $productsCache = [];

    public function __construct($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new \InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }


        $this->iblockId = $iblockId;

        $this->init();
    }

    private function init()
    {
        Loader::includeModule('catalog');

        $iterator = ProductTable::getList([
            'select' => [
                'ID',
                'WEIGHT',
                'WIDTH',
                'HEIGHT',
                'LENGTH'
            ]
        ]);

        while ($product = $iterator->Fetch()) {
            $this->productsCache[$product['ID']] = $product;
        }
    }

    public function getExisting($productId)
    {
        $existingProduct = $this->productsCache[$productId];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }
}