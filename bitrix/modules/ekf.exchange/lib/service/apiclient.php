<?php
namespace Ekf\Exchange\Service;

use Bitrix\Main\Web\Json;
use Ekf\Exchange\Exception\ApiEmptyResponseException;
use Ekf\Exchange\Exception\ApiException;

class ApiClient
{
    /** @var  string */
    private $accessToken;

    /** @var bool  */
    private $cacheMode;

    private $cacheDir;

    const BASE_URL = 'https://ekfgroup.com/api/v1/ekf';

    /**
     * ApiClient constructor.
     * @param $accessToken
     * @param bool $cacheMode - выдавать ответ из Кэша. Нужно для отладки и ускорения запросов.
     * @throws \Exception
     */
    public function __construct($accessToken, $cacheMode = false)
    {
        if (empty($accessToken)) {
            throw new \InvalidArgumentException('АПИ токен не может быть пустым');
        }

        if (defined('ekf.exchange.debug')) {
            $cacheMode = true;
        }

        $this->accessToken = $accessToken;

        $this->cacheMode = $cacheMode;
        if ($this->cacheMode) {
            $this->cacheDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/ekf_exchange_cache';
            if (!file_exists($this->cacheDir)) {
                $ok = mkdir($this->cacheDir);
                if (!$ok) {
                    throw new \Exception(sprintf('Не удалось создать директорию для кэша %s', $this->cacheDir));
                }
            }
        }
    }

    public function getCategories($pageNum = false, $limit = false)
    {
        $url = sprintf('%s/catalog/product-groups', self::BASE_URL);

        $categories = $this->request($url, $pageNum, $limit)['data'];

        if (!is_array($categories) || count($categories) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет категорий'));
        }

        return $categories;
    }

    public function getProperties()
    {
        $url = 'https://ekfgroup.com/api/v1/etim/features';

        $properties = $this->request($url)['data'];

        if (!is_array($properties) || count($properties) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет значений свойств'));
        }

        return $properties;
    }

    public function getPropertiesValues($pageNum = false, $limit = false, array $filter = [])
    {
        $url = sprintf('%s/catalog/properties', self::BASE_URL);

        $properties = $this->request($url, $pageNum, $limit, $filter)['data'];

        if (!is_array($properties) || count($properties) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет значений свойств'));
        }

        return $properties;
    }

    public function getProducts($pageNum = false, $limit = false, array $filter = [])
    {
        $url = sprintf('%s/catalog/products', self::BASE_URL);

        $products = $this->request($url, $pageNum, $limit, $filter)['data'];

        if (!is_array($products) || count($products) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет товаров'));
        }

        return $products;
    }

    public function getFiles($pageNum = false, $limit = false, array $filter = [])
    {
        $url = sprintf('%s/catalog/files', self::BASE_URL);

        $files = $this->request($url, $pageNum, $limit, $filter)['data'];

        if (!is_array($files) || count($files) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет файлов'));
        }

        return $files;
    }

    public function getPrices($pageNum = false, $limit = false)
    {
        $url = sprintf('%s/catalog/prices', self::BASE_URL);

        $prices = $this->request($url, $pageNum, $limit)['data'];

        if (!is_array($prices) || count($prices) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет цен'));
        }

        return $prices;
    }

    public function getWarehouses($pageNum = false, $limit = false)
    {
        $url = sprintf('%s/warehouses', self::BASE_URL);

        $warehouses = $this->request($url, $pageNum, $limit)['data'];

        if (!is_array($warehouses) || count($warehouses) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет складов'));
        }

        return $warehouses;
    }

    public function getRemains($pageNum = false, $limit = false, array $filter = [])
    {
        $url = sprintf('%s/stocks', self::BASE_URL);

        $remains = $this->request($url, $pageNum, $limit, $filter)['data'];

        if (!is_array($remains) || count($remains) == 0) {
            throw new ApiEmptyResponseException(sprintf('В ответе АПИ нет остатков'));
        }

        return $remains;
    }

    /**
     * @param $url
     * @param bool $pageNum
     * @param bool $limit
     * @param array $filter
     * @return mixed
     * @throws ApiException
     */
    private function request($url, $pageNum = false, $limit = false, array $filter = [])
    {
        // Запрос с пустым лимитом возвращает только тысячу записей
        $limit = (int)$limit == 0 ? 1000000 : $limit;

        if ($limit !== false && (int)$limit == 0) {
            throw new \InvalidArgumentException('$limit должен быть больше нуля');
        }

        if ($pageNum !== false && (int)$pageNum < 0) {
            throw new \InvalidArgumentException('$pageNum должен быть больше либо равен нулю');
        }

        $urlParams = [];

        if ($pageNum !== false) {
            $urlParams['page'] = $pageNum;
        }

        if ($limit !== false) {
            $urlParams['limit'] = $limit;
        }

        if (count($filter) > 0) {
            $urlParams += $filter;
        }

        if (count($urlParams) > 0) {
            $url .= '?' . http_build_query($urlParams);
        }

        $cacheFile = $this->cacheDir . '/' . md5($url);
        if ($this->cacheMode && file_exists($cacheFile)) {
            $jsonResponse = file_get_contents($cacheFile);
        } else {

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Accept: application/json',
                'Authorization: Bearer ' . $this->accessToken
            ]);

            $jsonResponse = curl_exec($curl);

            $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE );

            curl_close($curl);

            if ($responseCode != 200) {
                throw new ApiException(sprintf(
                        'Ошибка запроса к АПИ код ответа %s, %s', $responseCode, $url)
                );
            }

            if ($this->cacheMode) {
                file_put_contents($cacheFile, $jsonResponse);
            }
        }

        $response = Json::decode($jsonResponse);

        if (!array_key_exists('data', $response)) {
            throw new ApiException('Ошибка запроса к АПИ - в ответе нет обязательного ключа data');
        }

        return $response;
    }
}