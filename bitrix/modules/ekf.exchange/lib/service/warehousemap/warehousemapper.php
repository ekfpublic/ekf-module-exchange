<?php
namespace Ekf\Exchange\Service\WarehouseMap;

use Bitrix\Main\Loader;
use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;

/**
 * Служит для настройки соответствия складов сайта и АПИ
 */

class WarehouseMapper
{
    /** @var Config  */
    private $configManager;

    /** @var ApiClient  */
    private $apiClient;

    public function __construct()
    {
        $this->configManager = new Config();
        $this->apiClient = new ApiClient($this->configManager->getApiKey());

        Loader::includeModule('catalog');
    }

    public function show()
    {
        $apiWarehouses = $this->apiClient->getWarehouses();

        $siteWarehouses = $this->getSiteWarehouses();

        $warehousesMap = $this->configManager->getWarehousesMap();

        require(__DIR__ . '/templates/template.php');
    }

    private function getSiteWarehouses()
    {
        $iterator = \CCatalogStore::GetList(['NAME' => 'ASC'], ['ACTIVE' => 'Y', '!XML_ID' => false]);

        $warehouses = [];

        while ($warehouse = $iterator->Fetch()) {
            $warehouses[] = $warehouse;
        }

        return $warehouses;
    }
}