<?php
namespace Ekf\Exchange\Service;

use Ekf\Exchange\Entity\ProductEntity;
use Exception;
use InvalidArgumentException;

/**
 * Ищет товары в инфоблоке, чтобы понять, существует ли такой товар или нет
 */
class ProductFinder
{
    private $bInited = false;

    private $productsCache = [];

    /**
     * Способ сопоставления товаров (по артикулу, внешнему коду)
     * @var string
     */
    private $matchMethod;

    /**
     * @var array
     */
    private $propsMap;

    public function __construct(string $matchMethod, array $propsMap)
    {
        $this->matchMethod = $matchMethod;
        $this->propsMap = $propsMap;
    }

    /**
     * @param $iblockId
     */
    public function init($iblockId)
    {
        if ((int)$iblockId === 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        $this->fillExistingProductsMap($iblockId);

        $this->bInited = true;
    }

    /**
     * Получает
     *
     * @param ProductEntity $productEntity
     * @return array
     * @throws Exception
     */
    public function getExisting(ProductEntity $productEntity)
    {
        if (!$this->bInited) {
            throw new Exception('Сначал необходимо вызвать init()');
        }

        if ($this->matchMethod === Config::IDENTIFY_BY_VENDOR_CODE) {
            $key = $productEntity->getVendorCode();
        } else {
            $key = $productEntity->getXmlId();
        }

        $existingProduct = $this->productsCache[$key];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }

    /**
     * Заполняет массив данными о существующих товарах
     * @param $iblockId
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    private function fillExistingProductsMap($iblockId)
    {
        $vendorCodePropCode = $this->propsMap['vendorCode'];

        $select = array('ID', 'XML_ID', 'PROPERTY_EKF_HASH', 'PROPERTY_EKF_HASH_P', 'PROPERTY_EKF_HASH_F');

        if (!empty($vendorCodePropCode)) {
            $select[] = 'PROPERTY_' . $vendorCodePropCode;
        }

        $identifyByVendorCode = $this->matchMethod === Config::IDENTIFY_BY_VENDOR_CODE && 'PROPERTY_' . $vendorCodePropCode;
        if ($identifyByVendorCode) {
            $mapKey = 'PROPERTY_' . $vendorCodePropCode . '_VALUE';
        } else {
            $mapKey = 'XML_ID';
        }

        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            $select
        );

        $this->productsCache = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->productsCache[$arElement[$mapKey]] = $arElement;
        }
    }
}