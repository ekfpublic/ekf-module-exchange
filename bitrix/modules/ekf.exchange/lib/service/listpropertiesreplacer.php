<?php
namespace Ekf\Exchange\Service;

use Ekf\Exchange\Entity\ProductEntity;
use Ekf\Exchange\Entity\PropertyValueEntity;
use Ekf\Exchange\Repository\PropertyRepository;
use Exception;

/**
 * Заменяет строковые значения свойств типа список на их числовой идентификатор, пригодные для сохранения в БД
 */
class ListPropertiesReplacer
{
    private $bInited = false;

    private $propsCache = [];

    /**
     * Карта вида КОД_СВОЙСТВА => ИД_СВОЙСТВА
     * @var array
     */
    private $propertyCodeIdMap = [];

    /** @var PropertyRepository  */
    private $propertyRepository;

    public function __construct()
    {
        $this->propertyRepository = new PropertyRepository;
    }

    public function init(int $iblockId)
    {
        $this->fillPropertiesCache($iblockId);

        $this->bInited = true;
    }

    /**
     * @param ProductEntity $productEntity
     * @return ProductEntity
     * @throws Exception
     */
    public function withReplacedProperties(ProductEntity $productEntity)
    {
        if (!$this->bInited) {
            throw new Exception('Сначал необходимо вызвать init()');
        }

        /** @var PropertyValueEntity $propertyValue */
        foreach ($productEntity->getPropertyCollection() as $propertyValue) {
            if (empty($propertyValue->getValue())) {
                continue;
            }

            $propertyCode = $propertyValue->getPropertyCode();

            if (array_key_exists($propertyCode, $this->propsCache)) { // Это свойство-список

                $propertyId = $this->propertyCodeIdMap[$propertyCode];

                if ($this->propsCache[$propertyCode] === false) {
                    $this->propsCache[$propertyCode] = $this->getEnumValues($propertyId);
                }

                if ($propertyValue->getValue() == '') {
                    $propertyValue->setValue(false);
                } else {

                    $numericValue = $this->propsCache[$propertyCode][$propertyValue->getValue()];

                    if (!isset($numericValue)) {
                        $numericValue = $this->propertyRepository->addEnumValue($propertyId, $propertyValue->getValue());

                        if ($numericValue > 0) {
                            $this->propsCache[$propertyCode][$propertyValue->getValue()] = $numericValue;
                        }
                    }

                    if ($numericValue == 0) {
                        throw new \Exception(sprintf(
                            'Не удалось получить/создать числовое значение для свойства %s: %s',
                            $propertyValue->getPropertyCode(),
                            $propertyValue->getValue()
                        ));
                    }

                    $propertyValue->setValue($numericValue);
                }
            }
        }

        return $productEntity;
    }

    /**
     * Изначально заполняет массив ключами, соответствующим кодам свойств-списков
     * @param $iblockId
     */
    private function fillPropertiesCache($iblockId)
    {
        $allProperties = $this->propertyRepository->getProperties($iblockId);

        $this->propsCache = [];

        foreach ($allProperties as $property) {
            if ($property['PROPERTY_TYPE'] != 'L') {
                continue;
            }

            $this->propsCache[$property['CODE']] = false;
            $this->propertyCodeIdMap[$property['CODE']] = $property['ID'];
        }
    }

    /**
     * Заполняет соотвествия для свойств типа список [Значение => Идентификатор]
     * @param $propertyId
     * @return array
     */
    private function getEnumValues($propertyId)
    {
        $enumValues = $this->propertyRepository->getEnumValues($propertyId);

        $values = [];

        foreach ($enumValues as $enumValue) {
            $values[$enumValue['VALUE']] = $enumValue['ID'];
        }

        return $values;
    }
}