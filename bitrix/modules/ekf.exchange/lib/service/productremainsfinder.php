<?php
namespace Ekf\Exchange\Service;

use Bitrix\Catalog\ProductTable;
use Bitrix\Main\Loader;
use InvalidArgumentException;

class ProductRemainsFinder
{
    private $productsCache = [];

    /** @var Config  */
    private $config;

    /**
     * @var string
     */
    private $matchMethod;

    /**
     * @var array
     */
    private $propsMap;

    public function __construct($iblockId, string $matchMethod, array $propsMap)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        Loader::includeModule('catalog');
        Loader::includeModule('iblock');

        $this->matchMethod = $matchMethod;
        $this->propsMap = $propsMap;

        $this->config = new Config();

        $this->fillRemainsMap($iblockId);
    }

    /**
     * Заполняет массив данными о существующих ценах
     * @param $iblockId
     * @throws \Exception
     */
    private function fillRemainsMap($iblockId)
    {
        $this->productsCache = [];

        $warehouseMap = $this->config->getWarehousesMap();

        $warehouseIds = [];

        /**
         * Получим соответствие для товаров ID => XML_ID
         */
        $productsMap = [];

        $select = array('ID', 'XML_ID');

        $vendorCodePropCode = $this->propsMap['vendorCode'];

        if (!empty($vendorCodePropCode)) {
            $select[] = 'PROPERTY_' . $vendorCodePropCode;
        }

        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            $select
        );

        $identifyByVendorCode = $this->matchMethod === Config::IDENTIFY_BY_VENDOR_CODE && 'PROPERTY_' . $vendorCodePropCode;
        if ($identifyByVendorCode) {
            $mapKey = 'PROPERTY_' . $vendorCodePropCode . '_VALUE';
        } else {
            $mapKey = 'XML_ID';
        }

        while ($product = $dbElements->Fetch()) {
            $productsMap[$product['ID']] = $product[$mapKey];
            $this->productsCache[$product[$mapKey]]['product_id'] = $product['ID'];
        }

        /**
         * Получим ИД складов по их XML_ID
         */
        $iterator = \CCatalogStore::GetList([], ['XML_ID' => array_values($warehouseMap)]);

        $warehouseIdMap = [];

        while ($warehouse = $iterator->Fetch()) {
            $warehouseIds[] = $warehouse['ID'];
            $warehouseIdMap[$warehouse['ID']] = $warehouse['XML_ID'];
        }

        /**
         * Получим остатки по складам
         */
        $iterator = \CCatalogStoreProduct::GetList([], ['STORE_ID' => $warehouseIds], false, false, ['ID', 'PRODUCT_ID', 'STORE_ID', 'AMOUNT']);

        while ($remains = $iterator->Fetch()) {
            $productXmlId = $productsMap[$remains['PRODUCT_ID']];
            if ($productXmlId) {
                $warehouseXmlId = $warehouseIdMap[$remains['STORE_ID']];
                $this->productsCache[$productXmlId]['by_store'][$warehouseXmlId] = [
                    'amount' => (int)$remains['AMOUNT'],
                    'record_id' => $remains['ID']
                ];

                $this->productsCache[$productXmlId]['product_id'] = $remains['PRODUCT_ID'];
            }
        }

        /**
         * Получим суммарные остатки (они хранятся в отдельном поле)
         */
        $iterator = ProductTable::getList([
            'select' => [
                'ID',
                'QUANTITY'
            ]
        ]);

        while ($remains = $iterator->Fetch()) {
            $productXmlId = $productsMap[$remains['ID']];
            if ($productXmlId) {
                $this->productsCache[$productXmlId]['total'] = (int)$remains['QUANTITY'];
                $this->productsCache[$productXmlId]['product_id'] = $remains['ID'];
            }
        }
    }

    /**
     * @param $productXmlID
     * @return array
     */
    public function getExisting($productXmlID)
    {
        $existingProduct = $this->productsCache[$productXmlID];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }

    /**
     * Устанавливает признак того, что информацию о данном товаре была обновлена в БД
     * Это нужно для того, чтобы после обмена обнаружить, о каких товарах нет информации и обнулить остатки
     * @param $productXmlID
     */
    public function setRecordUpdatedFlag($productXmlID)
    {
        if (array_key_exists($productXmlID, $this->productsCache)) {
            $this->productsCache[$productXmlID]['updated'] = true;
        }
    }

    /**
     * Возвращает идентификаторы не обновленных товаров (именно ID, а не XML_ID)
     */
    public function getNotUpdatedProductIds()
    {
        $ids = [];

        foreach ($this->productsCache as $item) {
            if ($item['updated'] !== true) {
                $ids[] = $item['product_id'];
            }
        }

        return $ids;
    }
}