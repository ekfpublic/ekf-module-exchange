<?php
namespace Ekf\Exchange\Service\Install;

use Bitrix\Iblock\PropertyTable;
use Ekf\Exchange\Repository\PropertyRepository;
use Ekf\Exchange\Repository\UserFieldRepository;
use InvalidArgumentException;

/**
 * Отвечает за добавлений служебных свойств, нужных для работы модуля
 */
class PropertyManager
{
    /** @var PropertyRepository  */
    private $propertyRepository;

    /** @var UserFieldRepository  */
    private $userFieldRepository;

    public function __construct()
    {
        $this->propertyRepository = new PropertyRepository;
        $this->userFieldRepository = new UserFieldRepository();
    }
    public function installProperties($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        foreach ($this->getProperties($iblockId) as $property) {
            try {
                $this->propertyRepository->addProperty($property);
            } catch (\Exception $ex) {

            }
        }

        foreach ($this->getUserFields($iblockId) as $userField) {
            $this->userFieldRepository->addUserField($userField);
        }
    }

    public function uninstallProperties()
    {
        foreach ($this->getProperties(0) as $property) {
            try {
                $this->propertyRepository->deleteProperty($property['CODE']);
            } catch (\Exception $ex) {

            }
        }
    }

    private function getProperties($iblockId)
    {
        $properties = [
            // Хэш товара
            [
                'IBLOCK_ID' => $iblockId,
                'NAME' => 'EKF_HASH',
                'CODE' => 'EKF_HASH',
                'PROPERTY_TYPE' => PropertyTable::TYPE_STRING
            ],
            // Хэш свойств
            [
                'IBLOCK_ID' => $iblockId,
                'NAME' => 'EKF_HASH_P',
                'CODE' => 'EKF_HASH_P',
                'PROPERTY_TYPE' => PropertyTable::TYPE_STRING
            ],
            // Хэш файлов
            [
                'IBLOCK_ID' => $iblockId,
                'NAME' => 'EKF_HASH_F',
                'CODE' => 'EKF_HASH_F',
                'PROPERTY_TYPE' => PropertyTable::TYPE_STRING
            ],
            // Хэш файлов документации
            [
                'IBLOCK_ID' => $iblockId,
                'NAME' => 'EKF_HASH_D',
                'CODE' => 'EKF_HASH_D',
                'PROPERTY_TYPE' => PropertyTable::TYPE_STRING
            ],
            // Признак того, что товар обновлен
            [
                'IBLOCK_ID' => $iblockId,
                'NAME' => 'EKF_UPDATED',
                'CODE' => 'EKF_UPDATED',
                'PROPERTY_TYPE' => PropertyTable::TYPE_NUMBER
            ],
            // Признак того, что товар выгружен из EKF
            [
                'IBLOCK_ID' => $iblockId,
                'NAME' => 'EKF_IS_EKF',
                'CODE' => 'EKF_IS_EKF',
                'PROPERTY_TYPE' => PropertyTable::TYPE_NUMBER
            ]
        ];

        return $properties;
    }

    protected function getUserFields($iblockId)
    {
        $aUserFields = [
            // Признак того, что раздел EKF (т.к. на сайте кроме разделов EKF могут быть разделы с продукцией других поставщиков)
            [
                'ENTITY_ID'     => "IBLOCK_{$iblockId}_SECTION",
                'FIELD_NAME'    => 'UF_IS_EKF',
                'USER_TYPE_ID'  => 'integer',
                'XML_ID'        => 'UF_IS_EKF',
                'SETTINGS'      => [
                    'DEFAULT_VALUE' => 0
                ]
            ],

            // Хэш данных раздела
            [
                'ENTITY_ID'     => "IBLOCK_{$iblockId}_SECTION",
                'FIELD_NAME'    => 'UF_EKF_HASH',
                'USER_TYPE_ID'  => 'string',
                'XML_ID'        => 'UF_EKF_HASH'
            ]
        ];

        return $aUserFields;
    }
}