<?php
namespace Ekf\Exchange\Service;

use Bitrix\Main\Loader;
use CFile;
use InvalidArgumentException;

class ProductImagesFinder
{
    private $productsCache = [];

    /**
     * @var string
     */
    private $matchMethod;

    /**
     * @var array
     */
    private $propsMap;

    public function __construct($iblockId, string $matchMethod, array $propsMap)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        Loader::includeModule('iblock');

        $this->matchMethod = $matchMethod;
        $this->propsMap = $propsMap;

        $this->fillExistingProductsMap($iblockId);
    }

    /**
     * Заполняет массив данными о существующих товарах
     * Нам нужны названия файлов, т.к. в АПИ название файла - это его хэш
     * Сравнив название файла на сайте и в АПИ можно понять, изменился он или нет
     * @param $iblockId
     */
    private function fillExistingProductsMap($iblockId)
    {
        $select = array('ID', 'XML_ID', 'PROPERTY_EKF_HASH_F', 'PROPERTY_EKF_HASH_D', 'DETAIL_PICTURE');

        $vendorCodePropCode = $this->propsMap['vendorCode'];

        if (!empty($vendorCodePropCode)) {
            $select[] = 'PROPERTY_' . $vendorCodePropCode;
        }

        $identifyByVendorCode = $this->matchMethod === Config::IDENTIFY_BY_VENDOR_CODE && 'PROPERTY_' . $vendorCodePropCode;
        if ($identifyByVendorCode) {
            $mapKey = 'PROPERTY_' . $vendorCodePropCode . '_VALUE';
        } else {
            $mapKey = 'XML_ID';
        }

        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            $select
        );

        $detailPicMap = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->productsCache[$arElement[$mapKey]] = $arElement;

            if (!empty($arElement['DETAIL_PICTURE'])) {
                $detailPicMap[$arElement['DETAIL_PICTURE']] = $arElement[$mapKey];
            }
        }

        $chunks = array_chunk($detailPicMap, 1000, true);

        foreach ($chunks as $chunk) {
            $filesIterator = CFile::GetList([], ['@ID' => array_keys($chunk)]);
            while ($file = $filesIterator->Fetch()) {
                $productXmlId = $detailPicMap[$file['ID']];
                $this->productsCache[$productXmlId]['DETAIL_PICTURE_EXTERNAL_ID'] = $file['EXTERNAL_ID'];
            }
        }
    }

    /**
     * @param $productXmlID
     * @return array
     */
    public function getExisting($productXmlID)
    {
        $existingProduct = $this->productsCache[$productXmlID];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }
}