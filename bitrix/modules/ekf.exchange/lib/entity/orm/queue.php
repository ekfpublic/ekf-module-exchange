<?php
namespace Ekf\Exchange\Entity\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\TextField;

/**
 * Таблица-очередь для импорта данных
 */

class QueueTable  extends DataManager
{
    public static function getTableName()
    {
        return 'ekf_exchange_queue';
    }

    public static function getMap()
    {
        return [
            new IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new StringField('product_id'),
            new TextField('data'),
            new StringField('type'),
            new IntegerField('updated'),
        ];
    }
}