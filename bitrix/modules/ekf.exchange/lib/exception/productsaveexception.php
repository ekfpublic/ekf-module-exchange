<?php
namespace Ekf\Exchange\Exception;

use Exception;

/**
 * Ошибка сохранения данных товара в БД
 */
class ProductSaveException extends Exception
{

}