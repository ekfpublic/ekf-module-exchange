<?php
namespace Ekf\Exchange\Exception;

use Exception;

/**
 * Формат данных об свойстве товара, полученном через АПИ, не соответствует ожидаемому
 * Например, не заполнено какое-либо поле
 */
class BadPropertyDataException extends Exception
{

}