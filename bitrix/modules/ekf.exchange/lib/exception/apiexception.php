<?php
namespace Ekf\Exchange\Exception;

use Exception;

/**
 * Ошибка запроса к АПИ
 */
class ApiException extends Exception
{

}