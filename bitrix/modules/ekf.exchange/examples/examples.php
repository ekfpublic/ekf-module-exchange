<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 * Добавляем новое свойство товару в обработчике
 */
$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('ekf.exchange', 'after_product_entity_created', function (\Bitrix\Main\Event $event) {
    /** @var \Ekf\Exchange\Entity\ProductEntity $productEntity */
    $productEntity = $event->getParameter('productEntity');

    /** @var array $productApiData */
    $productApiData = $event->getParameter('productApiData');

    $propertyValueFactory = new Ekf\Exchange\Factory\PropertyValueFactory();

    $productEntity->setPreviewText('');
    $propertyEntity = $propertyValueFactory->buildFromArray([
        'api_code' => 'PROP_CODE',
        'value'    => $productApiData['PROPERTY_AAA']
    ]);

    $productEntity->addToPropertyCollection($propertyEntity);

    return $productEntity;
});
