<?php
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Ekf\Exchange\Service\SectionMap\SectionMapper;

defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'ekf.exchange');

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('');
}

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('ekf.exchange');

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

$configManager = new \Ekf\Exchange\Service\Config();

Loc::loadMessages($context->getServer()->getDocumentRoot() . '/bitrix/modules/main/options.php');
Loc::loadMessages(__FILE__);

try {
    (new \Ekf\Exchange\Service\RequirementsChecker())->checkModuleRequirements();
} catch (\Ekf\Exchange\Exception\ModuleRequirementsException $ex) {
    CAdminMeShowError('Работа модуля невозможна: ' . $ex->getMessage());
    return;
}
/**
 * Сохранение настроек
 */

if ($request->isPost() && $request->getPost("edit_ekf_exchange_options") === "Y" && check_bitrix_sessid()){
    try {
        $data = $request->get('ekf_exchange');

        $configManager->saveIblockId((int)$data['iblock']);
        $configManager->saveApiKey($data['api_key']);
        $configManager->saveMorePhotoPropertyCode($data['more_photo_property']);
        $configManager->saveDocsPropertyCode($data['docs_files_property']);
        $configManager->savePropertiesMap($data['property_map']);

        $configManager->savePricesMap($data['price_map']);
        $configManager->savePriceDiscountsMap($data['discount_map']);
        $configManager->saveWarehousesMap($data['warehouses_map']);
        $configManager->saveSectionsMap($data['sections_map']);
        $configManager->saveProductIdentifyMethod($data['product_identify_method']);

        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
    } catch (Exception $ex) {
        CAdminMessage::showMessage(array(
            "MESSAGE" => sprintf('%s', $ex->getMessage()),
            "TYPE" => "ERROR",
        ));
    }
}

/**
 * Вкладки
 */
$tabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => "Общие",
        "TITLE" => "Общие",
    )
);

if (!empty($configManager->getApiKey()) && $configManager->getIblockId() > 0) {
    $tabs[] = array(
        "DIV" => "edit2",
        "TAB" => "Разделы",
        "TITLE" => "Разделы",
    );

    $tabs[] = array(
        "DIV" => "edit3",
        "TAB" => "Свойства",
        "TITLE" => "Свойства",
    );

    $tabs[] = array(
        "DIV" => "edit4",
        "TAB" => "Цены",
        "TITLE" => "Цены",
    );

    $tabs[] = array(
        "DIV" => "edit5",
        "TAB" => "Остатки",
        "TITLE" => "Остатки",
    );

    /**
     * Не очень рационально при каждом открытии создавать свойства, логичнее делать на этапе установки
     * Но тогда придется выбор инфоблока делать так же на этапе установки, и делать это не хочется
     * В любом случает свойства пересоздаваться не будут, если уже созданы
     */
    (new \Ekf\Exchange\Service\Install\PropertyManager())->installProperties($configManager->getIblockId());
}

$tabControl = new CAdminTabControl("tabControl", $tabs);

$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID)?>">

    <input type="hidden" name="edit_ekf_exchange_options" value="Y" />
    <?= bitrix_sessid_post() ?>

    <? $tabControl->beginNextTab(); ?>

    <tr class="heading">
        <td colspan="2">Убедитесь, что у свойств заполнено поле "Символьный код". Свойства без кода не могуть обновляться через модуль.</td>
    </tr>

    <tr>
        <td width="40%">
            <label for="max_image_size">Инфоблок для загрузки товаров:</label>
        </td>
        <td width="60%">
            <?
            $iblocksIterator = \Bitrix\Iblock\IblockTable::getList([
                'order' => [
                    'NAME' => 'ASC'
                ]
            ]);
            ?>

            <select name="ekf_exchange[iblock]">

            <? while ($iblock = $iblocksIterator->fetch()) { ?>

                <option <?= ($iblock['ID'] == $configManager->getIblockId()) ? 'selected' : '' ?> value="<?= $iblock['ID'] ?>"><?= $iblock['NAME'] ?> [<?= $iblock['ID'] ?>]</option>

            <? } ?>

            </select>
        </td>
    </tr>

    <tr>
        <td width="40%">
            <label for="max_image_size">Ключ АПИ:</label>
        </td>
        <td width="60%">
            <textarea name="ekf_exchange[api_key]" id="" cols="60" rows="3"><?= $configManager->getApiKey() ?></textarea>
        </td>
    </tr>

    <? if ($configManager->getIblockId() > 0) { ?>

    <tr>
        <td width="40%">
            <label for="max_image_size">Свойство для загрузки дополнительных изображений:<br>(множественное, тип - файл)</label>
        </td>
        <td width="60%">
            <?
            $propertiesIterator = \Bitrix\Iblock\PropertyTable::getList([
                'order' => [
                    'NAME' => 'ASC',
                ],
                'filter' => [
                    'IBLOCK_ID' => $configManager->getIblockId(),
                    'ACTIVE' => 'Y',
                    '!CODE' => false,
                    'PROPERTY_TYPE' => \Bitrix\Iblock\PropertyTable::TYPE_FILE,
                    'MULTIPLE' => 'Y'
                ]
            ]);

            if ($propertiesIterator->getSelectedRowsCount() == 0) {
                CAdminMessage::showMessage(array(
                    "MESSAGE" => "Для загрузки доп. изображений должно быть создано множественное свойство типа файл",
                    "TYPE" => "ERROR",
                ));
            }

            $multipleFileProperties = [];

            while ($property = $propertiesIterator->fetch()) {
                $multipleFileProperties[] = $property;
            }
            ?>

            <select name="ekf_exchange[more_photo_property]">

                <? foreach ($multipleFileProperties as $property) { ?>

                    <option <?= ($property['CODE'] == $configManager->getMorePhotoPropertyCode()) ? 'selected' : '' ?> value="<?= $property['CODE'] ?>"><?= $property['NAME'] ?> [<?= $property['ID'] ?>]</option>

                <? } ?>

            </select>
        </td>
    </tr>

        <tr>
            <td width="40%">
                <label for="max_image_size">Свойство для загрузки файлов документации:<br>(множественное, тип - файл, с признаком - Выводить поле для описания)</label>
            </td>
            <td width="60%">
                <select name="ekf_exchange[docs_files_property]">

                    <option value="">Не выгружать</option>

                    <? foreach ($multipleFileProperties as $property) { ?>

                        <option <?= ($property['CODE'] == $configManager->getDocsPropertyCode()) ? 'selected' : '' ?> value="<?= $property['CODE'] ?>"><?= $property['NAME'] ?> [<?= $property['ID'] ?>]</option>

                    <? } ?>

                </select>
            </td>
        </tr>

        <tr>
            <td width="40%">
                <label for="max_image_size">Способ сопоставления товаров:</label>
            </td>
            <td width="60%">

                <?php
                $methods = [
                    \Ekf\Exchange\Service\Config::IDENTIFY_BY_XML_ID => 'Внешний код (XML_ID)',
                    \Ekf\Exchange\Service\Config::IDENTIFY_BY_VENDOR_CODE => 'Артикул',
                ];

                $currentMethod = $configManager->getProductIdentifyMethod();
                ?>

                <select name="ekf_exchange[product_identify_method]">

                    <?php foreach ($methods as $code => $title) { ?>

                        <option <?= ($code === $currentMethod) ? 'selected' : '' ?> value="<?= $code ?>"><?= $title ?></option>

                    <?php } ?>

                </select>
            </td>
        </tr>

    <? } ?>

    <? $tabControl->beginNextTab(); ?>

    <? if (!empty($configManager->getApiKey()) && $configManager->getIblockId() > 0) { ?>
        <!-- Настройки доступны только после ввода ключа, т.к. требуют вызова АПИ -->

        <tr>
            <td colspan="2">

            <?
            try {
                $categoryMapper = new SectionMapper();
               $categoryMapper->show();
            } catch (Exception $ex) {
                CAdminMessage::showMessage(array(
                    "MESSAGE" => sprintf('%s', $ex->getMessage()),
                    "TYPE" => "ERROR",
                ));
            }
            ?>

            </td>
        </tr>

    <? } ?>

    <? $tabControl->beginNextTab(); ?>

    <? if (!empty($configManager->getApiKey()) && $configManager->getIblockId() > 0) { ?>

    <tr>
        <td colspan="2">
            <?
            try {
                $propertiesMapper = new \Ekf\Exchange\Service\PropertyMap\PropertyMapper();
                $propertiesMapper->show();
            } catch (Exception $ex) {
                CAdminMessage::showMessage(array(
                    "MESSAGE" => sprintf('%s', $ex->getMessage()),
                    "TYPE" => "ERROR",
                ));
            }
            ?>
        </td>
    </tr>

    <? } ?>

    <? $tabControl->beginNextTab(); ?>

    <? if (!empty($configManager->getApiKey()) && $configManager->getIblockId() > 0) { ?>

    <tr>
        <td colspan="2">
            <?
            try {
                $priceMapper = new \Ekf\Exchange\Service\PriceMap\PriceMapper();
                $priceMapper->show();
            } catch (Exception $ex) {
                CAdminMessage::showMessage(array(
                    "MESSAGE" => sprintf('%s', $ex->getMessage()),
                    "TYPE" => "ERROR",
                ));
            }
            ?>
        </td>
    </tr>

    <? } ?>

    <? $tabControl->beginNextTab(); ?>

    <? if (!empty($configManager->getApiKey()) && $configManager->getIblockId() > 0) { ?>

    <tr>
        <td colspan="2">
            <?
            try {
                $warehouseMapper = new \Ekf\Exchange\Service\WarehouseMap\WarehouseMapper();
                $warehouseMapper->show();
            } catch (Exception $ex) {
                CAdminMessage::showMessage(array(
                    "MESSAGE" => sprintf('%s', $ex->getMessage()),
                    "TYPE" => "ERROR",
                ));
            }
            ?>
        </td>
    </tr>

    <? } ?>

    <? $tabControl->buttons(); ?>

    <input  type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>">
    <input type="hidden" name="Update" value="Y">

    <?php /*
    <input type="submit"
           name="save"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"/>
           */ ?>

    <? $tabControl->end(); ?>
</form>