<?php
/**
 * Обновление структуры разделов
 * @global CMain $APPLICATION
 * @var \Ekf\Exchange\Service\Output $output
 */
use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;
use Ekf\Exchange\Service\Logger;
use Ekf\Exchange\Service\SectionMapHelper;
use Ekf\Exchange\Functions\Categories as CatFunctions;
use Ekf\Exchange\Functions\Events as EventFunctions;


//todo::Оптимизировать, заранее получать все разделы и обновлять только если что-то изменилось
// Не критично, разделов не так и много, задача не первоочередная

require(__DIR__ . '/init.php');

try {

    $configManager = new Config();

    $logger = new Logger('categories');

    $logger->state('Начат обмен разделами');

    $apiClient = new ApiClient($configManager->getApiKey());

    $sectionMapper = new SectionMapHelper();

    $iblockId = $configManager->getIblockId();

    $apiSections = CatFunctions\getSortedApiSection($apiClient);

    $totalRecords = count($apiSections);

    $siteSectionsList = CatFunctions\getFullSiteSectionsList($iblockId);

    foreach ($apiSections as $apiSection) {
        try {
            if (PHP_SAPI == 'cli') {
                echo $apiSection['id'] . ' / ' . ($totalRecords--) . PHP_EOL;
                ob_end_flush();
            }

            // Событие перед созданием объекта раздела
            $modifiedData = EventFunctions\categoryBeforeDataParsed($apiSection);
            if (count($modifiedData) > 0) {
                $apiSection = $modifiedData;
            }

            $mapSection = $sectionMapper->getMatchedSection($apiSection['id']);

            if (count($mapSection) == 0) {
                continue;
            }

            $mapSectionId = $mapSection['id'];

            if ($mapSection['direct'] || $mapSection['collapse']) {
                CatFunctions\setSectionUpdated($siteSectionsList, $mapSectionId);
                continue;
            }

            if ($mapSection['direct_parent']) {
                $parentId = $mapSectionId;
                CatFunctions\setSectionUpdated($siteSectionsList, $parentId);
            } else {
                $parent = CatFunctions\getSectionByXmlId($iblockId, $apiSection['parentId']);
                $parentId = (int)$parent['ID'];
            }

            if ($parentId == 0) {
                throw new Exception(sprintf('Не удалось найти раздел по XML_ID %s', print_r($apiSection, true)));
            }

            $existingSection = CatFunctions\getSectionByXmlId($iblockId, $apiSection['id']);

            if ((int)$existingSection['ID'] == 0) {
                $code = $apiSection['bitrix_code'];
                if (empty($code)) {
                    $code = \CUtil::translit($apiSection['name'], 'ru', [
                        'replace_space' => '-',
                        'replace_other' => '-',
                    ]);

                    $code .= '-' . substr($apiSection['id'], 0, 4);
                }

                // При создании раздела хэш не заполняется (UF_EKF_HASH), так что одно обновление пройдет в любом случае, даже если данные не изменились
                $sectionId = CatFunctions\createSection(
                    $iblockId, $parentId, $apiSection['name'], $apiSection['id'], $code, $apiSection['mainImage']
                );
            } else {
                $sectionUpdateFields = [
                    'NAME'              => $apiSection['name'],
                    'IBLOCK_SECTION_ID' => $parentId,
                    'ACTIVE'            => 'Y',
                    '+IMG'              => $apiSection['mainImage'],
                ];

                $sectionHash = md5(serialize($sectionUpdateFields));

                if ($sectionHash != $existingSection['UF_EKF_HASH']) {
                    $sectionUpdateFields['UF_EKF_HASH'] = $sectionHash;
                    CatFunctions\updateSection($existingSection['ID'], $sectionUpdateFields);
                }

                $sectionId = $existingSection['ID'];
            }

            CatFunctions\setSectionUpdated($siteSectionsList, $sectionId);

        } catch (Exception $ex) {
            $output->writeLn($ex->getMessage());
            $logger->error($ex->getMessage());
        }
    }

    $logger->info('Деактивация отсутствующих разделов');

    CatFunctions\deactivateNonExistingSections($siteSectionsList);

    $logger->state('Завершен обмен разделами');

    $output->writeLn('done');

} catch (\Exception $ex) {
    $output->writeLn($ex->getMessage());

    $logger->critical($logger->formatException($ex), [
        'trace' => $ex->getTraceAsString()
    ]);
}