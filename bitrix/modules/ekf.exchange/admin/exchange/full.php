<?php
/**
 * Запускает полный обмен
 */

require(__DIR__ . '/init.php');

$scripts = [
    [
        'script' => 'categories.php',
        'title' => 'Разделы'
    ],
    [
        'script' => 'products.php',
        'title' => 'Товары',
        'mode' => 'products'
    ],
    [
        'script' => 'products.php',
        'title' => 'Характеристики товаров',
        'mode' => 'properties'
    ],
    [
        'script' => 'files.php',
        'title' => 'Изображения'
    ],
    [
        'script' => 'remains.php',
        'title' => 'Остатки'
    ],
    [
        'script' => 'prices.php',
        'title' => 'Цены'
    ]
];

foreach ($scripts as $script) {
    $fullScriptPath = __DIR__ . '/' . $script['script'];

    if (!empty($script['mode'])) {
        $_GET['mode'] = $script['mode'];
    }

    echo sprintf(
        'Начат обмен: %s%sСкрипт: %s%s', $script['title'], PHP_EOL, $fullScriptPath, PHP_EOL
    );

    require $fullScriptPath;

    echo PHP_EOL . PHP_EOL;
}