<?php
/**
 * @global CMain $APPLICATION
 */
use Ekf\Exchange\Repository\QueueRepository;
use Ekf\Exchange\Service\Config;
use Ekf\Exchange\Service\DataDownloader;

require(__DIR__ . '/init.php');

try {
    $logger = new \Ekf\Exchange\Service\Logger('remains');

    $warehouseMap = (new Config())->getWarehousesMap();

    $logger->state('Начат обмен остатками');

    if (empty($warehouseMap)) {
        throw new \Exception('Не заполнена карта складов');
    }

    $queueRepository = new QueueRepository();

    $remainsUpdater = new \Ekf\Exchange\Service\RemainsUpdater();

    $remainsFinder = new \Ekf\Exchange\Service\ProductRemainsFinder(
        (new Config())->getIblockId(),
        $configManager->getProductIdentifyMethod(),
        $configManager->getPropertiesMap()
    );

    /**
     * Загрузка данных из АПИ в таблицу-очередь
     */
    $totalRecords = (new DataDownloader)->loadDataToQueue(
        [$apiClient, 'getRemains'], false, 'remains'
    );

    $totalRecords = $queueRepository->getQueueSize();

    $identifyByVendorCode = $configManager->getProductIdentifyMethod() === Config::IDENTIFY_BY_VENDOR_CODE;

    do {
        try {
            $records = $queueRepository->getFromQueueByProduct(1000);

            foreach ($records as $productXmlId => $remainsForProduct) {

                try {
                    echo $productXmlId . ' / ' . $totalRecords . PHP_EOL;
                    ob_end_flush();

                    $totalProductQuantity = 0;

                    // [ информация о текущих остатках
                    $firstProduct = reset($remainsForProduct);
                    $firstProductData = unserialize($firstProduct['data'], ['allowed_classes' => false]);
                    $existingQuantityData = $remainsFinder->getExisting($identifyByVendorCode ? $firstProductData['vendorCode'] : $firstProductData['id']);
                    // ]

                    $siteProductId = (int)$existingQuantityData['product_id'];

                    if ($siteProductId === 0) {
                        continue;
                    }

                    $remainsFinder->setRecordUpdatedFlag($productXmlId);

                    foreach ($remainsForProduct as $remainsOnWarehouse) {
                        $totalRecords--;

                        $data = unserialize($remainsOnWarehouse['data'], ['allowed_classes' => false]);

                        if (!is_array($data) || count($data) === 0) {
                            throw new \Exception(sprintf('Поле data должно быть сериализованным массивом: %s', $remainsOnWarehouse['data']));
                        }

                        $apiWarehouseId = $data['warehouse']['id'];

                        // Если в настройках стоит обновление остатков по этому складу
                        if (array_key_exists($apiWarehouseId, $warehouseMap)) {
                            $quantity = (int)$data['quantity'];
                            $totalProductQuantity += $quantity;

                            $siteWarehouseXmlId = $warehouseMap[$apiWarehouseId];

                            if ($quantity != $existingQuantityData['by_store'][$siteWarehouseXmlId]['amount']) {
                                // Ид записи в таблице остатков. 0 - если записи нет
                                // Это помоает сразу понять, добавлять запись или обновлять
                                $recordId = (int)$existingQuantityData['by_store'][$siteWarehouseXmlId]['record_id'];
                                $remainsUpdater->updateWarehouseQuantity($siteProductId, $siteWarehouseXmlId, $quantity, $recordId);

                                $logger->info(sprintf(
                                    'Для товара %s [%s] обновлено количество на складе %s', $productXmlId, $siteProductId, $siteWarehouseXmlId
                                ));
                            }
                        }
                    }

                    // Обновим суммарное количество товара
                    if ($totalProductQuantity != (int)$existingQuantityData['total']) {
                        // Проверяем, существует ли запись в таблице каталога
                        // Это помоает сразу понять, добавлять запись или обновлять
                        $updateMode = isset($existingQuantityData['total']);
                        $remainsUpdater->updateTotalQuantity($siteProductId, $totalProductQuantity, $updateMode);

                        $logger->info(sprintf(
                            'Для товара %s [%s] обновлено суммарное количество', $productXmlId, $siteProductId
                        ));
                    }

                } catch (Exception $ex) {
                    $logger->critical($logger->formatException($ex), [
                        'product_id' => $product['id'],
                        'trace'      => $ex->getTraceAsString()
                    ]);
                }

            }

        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }

        //TODO::remove
        //break;

    } while (count($records) > 0);

    $notUpdatedIds = $remainsFinder->getNotUpdatedProductIds();

    foreach ($notUpdatedIds as $productId) {
        try {
            $remainsUpdater->clearRemains($productId);
            $logger->info(sprintf(
                'Остатки для товара %d обнулены', $productId
            ));
        } catch (Exception $ex) {
            $logger->error($logger->formatException($ex), [
                'trace' => $ex->getTraceAsString()
            ]);
        }

    }

} catch (Exception $ex) {
    echo $ex->getMessage(); // На случай если сам логгер выбросил исключение

    $logger->critical($logger->formatException($ex), [
        'trace' => $ex->getTraceAsString()
    ]);
}

$logger->state('Завершен обмен остатками');