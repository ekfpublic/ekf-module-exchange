<?php
use Bitrix\Main\Loader;
use Ekf\Exchange\Exception\ModuleRequirementsException;
use Ekf\Exchange\Service\RequirementsChecker;

if (!defined('ekf.exchange.inited')) {

    define('ekf.exchange.inited', true);

    $_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../../../../../');

    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

    ini_set('memory_limit', '512M');

    // Вспомогательная константа, нужна в коде проекта, чтобы понять, что идет обмен
    define('ekf.exchange', true);

    // Режим отладки. Запросы к АПИ кэшируются и отдаются из кэша. Нужно для ускорения отладки.

    if($argv!=null) {
        if (in_array('--api-cache', $argv)) {
            define('ekf.exchange.debug', true);
        }

        // В этом режиме вообще не делаем запросы к АПИ, просто получаем старые данные из таблицы-очереди
        if (in_array('--db-cache', $argv)) {
            define('ekf.exchange.dbcache', true);
        }
    }

    set_time_limit(0);

    ignore_user_abort(true);

    // Принудительное обновление без проверки - изменилось что-то или нет
    $forceUpdate = false;

    ob_implicit_flush();

    Loader::includeModule('ekf.exchange');
    Loader::includeModule('iblock');
    Loader::includeModule('sale');
    Loader::includeModule('catalog');

    try {
        (new RequirementsChecker())->checkModuleRequirements();
    } catch (ModuleRequirementsException $ex) {
        exit($ex->getMessage());
    }

    $configManager = new \Ekf\Exchange\Service\Config();

    $apiClient = new \Ekf\Exchange\Service\ApiClient($configManager->getApiKey());

    $output = new \Ekf\Exchange\Service\Output();

    $iblockId = $configManager->getIblockId();

    if ($configManager->getProductIdentifyMethod() === \Ekf\Exchange\Service\Config::IDENTIFY_BY_VENDOR_CODE) {
        $propsMap = $configManager->getPropertiesMap();
        if (empty($propsMap['vendorCode'])) {
            exit('Настроен поиск соответствий по артикулу, но в настройках не задано свойство для импорта артикула');
        }
    }
}