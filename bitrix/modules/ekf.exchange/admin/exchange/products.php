<?php
/**
 * @var Config $configManager
 * @var bool $forceUpdate
 */

use Ekf\Exchange\Entity\ProductEntity;
use Ekf\Exchange\Exception\BadProductDataException;
use Ekf\Exchange\Exception\NoMatchingSectionException;
use Ekf\Exchange\Exception\ProductSaveException;
use Ekf\Exchange\Factory\ProductFactory;
use Ekf\Exchange\Factory\ProductPropertyFactory;
use Ekf\Exchange\Factory\PropertyValueFactory;
use Ekf\Exchange\Repository\ProductRepository;
use Ekf\Exchange\Repository\QueueRepository;
use Ekf\Exchange\Service\CatalogProductFinder;
use Ekf\Exchange\Service\ChangesDetector;
use Ekf\Exchange\Service\Config;
use Ekf\Exchange\Service\DataDownloader;
use Ekf\Exchange\Functions;
use Ekf\Exchange\Functions\Events as EventFunctions;
use Ekf\Exchange\Service\ListPropertiesReplacer;
use Ekf\Exchange\Service\Logger;
use Ekf\Exchange\Service\ProductDataMapper;
use Ekf\Exchange\Service\ProductFinder;

require(__DIR__ . '/init.php');

try {
    $logger = new Logger('default');

    /**
     * Configure
     * Обмены товарами и свойствами (значениями свойств) очень похожи, поэтому сделаны одним файлом
     */
    $mode = '-----';

    $availableModes = ['products', 'properties'];

    if (isset($argv[1])) {
        $mode = $argv[1];
    }

    if (isset($_GET['mode'])) {
        $mode = $_GET['mode'];
    }

    if (!in_array($mode, $availableModes)) {
        throw new Exception(sprintf(
            'Неизвестный режим обмена - %s', $mode
        ));
    }

    $bProdMode = ($mode == 'products');

    $logger = new Logger($mode);

    $logger->state(sprintf(
        'Начат обмен %s', ($mode == 'products') ? 'товарами' : 'свойствами'
    ));

    $lineEnding = PHP_SAPI == 'cli' ? PHP_EOL : '<br>';

    $exchangeStat = [
        'processed' => 0,
        'updated' => 0,
        'created' => 0,
        'deactivated' => 0,
        'errors' => 0,
    ];

    /**
     * Construct
     */

    $logger->state('Инициализация модулей');

    $propertyValueFactory = new PropertyValueFactory();

    $listPropertiesReplacer = new ListPropertiesReplacer();

    $productRepository = new ProductRepository();

    $productDataMapper = new ProductDataMapper();

    $productFinder = new ProductFinder(
        $configManager->getProductIdentifyMethod(),
        $configManager->getPropertiesMap()
    );

    $catalogProductFinder = new CatalogProductFinder($iblockId);

    $changesDetector = new ChangesDetector();

    $queueRepository = new QueueRepository();


    /**
     * Initialize
     */
    $productFinder->init($iblockId);

    $listPropertiesReplacer->init($iblockId);

    $totalProductsProcessed = 0;

    $logger->state('Загрузка данных и постановка их в очередь');

    if ($mode == 'properties') {
        $hashPropCode = 'EKF_HASH_P';

        $productFactory = new ProductPropertyFactory();

        $totalRecords = (new DataDownloader)->loadDataToQueue(
            [$apiClient, 'getPropertiesValues'], false, 'property'
        );

    } else if ($mode == 'products') {
        $hashPropCode = 'EKF_HASH';

        $productFactory = new ProductFactory();

        $changesDetector->resetUpdatedFlag($iblockId);

        $totalRecords = (new DataDownloader)->loadDataToQueue(
            [$apiClient, 'getProducts'], false, 'product'
        );
    } else {
        throw new Exception('Неизвестный режим обмена');
    }

    if ($totalRecords == 0) {
        throw new Exception('В очередь поставлено 0 записей');
    }

    $logger->state('Обработка записей из очереди');

    /**
     * Process
     */
    do {
        $records = $queueRepository->getFromQueue(500);

        foreach ($records as $record) {

            $exchangeStat['processed']++;

            try {
                $productData = [];
                $productEntity = new ProductEntity();

                $productData = unserialize($record['data'], ['allowed_classes' => false]);

                if ($bProdMode) {

                    if (empty($productData['productGroup']['id'])) {
                        continue;
                    }

                    $productData['volume'] = $productData['unitStorage']['volume'];
                    $productData['bazoved'] = $productData['unitStorage']['name'];
                    $productData['multiplicity'] = (int)$productData['retailRate'];

                    $productData['multiplicity'] = $productData['multiplicity'] > 0
                        ? $productData['multiplicity']
                        : 1;
                }

                if (PHP_SAPI === 'cli') {
                    echo $productData['id'] . ' / ' . ($totalRecords--) . $lineEnding;
                    ob_end_flush();
                }

                // Событие перед созданием объекта товара
                $modifiedData = EventFunctions\productBeforeDataParsed($productData);
                if (count($modifiedData) > 0) {
                    $productData = $modifiedData;
                }

                /**
                 * Создаем объект товара
                 */
                $productEntity = $productFactory->buildFromArray($productData);
                if (!$productEntity) {
                    continue;
                }

                $productEntity->setIblockId($iblockId);

                /**
                 * Подставляем раздел и свойства из настроек
                 */
                $productEntity = $productDataMapper->withMappedData($productEntity);

                /**
                 * Заменяем текстовые значения свойств-списков на их идентификаторы
                 */
                $productEntity = $listPropertiesReplacer->withReplacedProperties($productEntity);

                if ($bProdMode) {
                    $code = Functions\Util\getProductCode($productEntity->getName());
                    $productEntity->setCode($code);
                }

                /**
                 * Рассчитаем хэш для того, чтобы не делать лишних обновлений, если ничего не изменилось
                 */
                $productDataHash = $changesDetector->getHash($productEntity);

                $productEntity->addToPropertyCollection(
                    $propertyValueFactory->buildFromArray([
                        'code'  => $hashPropCode,
                        'value' => $productDataHash
                    ])
                );

                $productEntity->addToPropertyCollection(
                    $propertyValueFactory->buildFromArray([
                        'code'  => 'EKF_UPDATED',
                        'value' => 1
                    ])
                );

                // Событие после создания сущности товара
                $modifiedProductEntity = EventFunctions\productAfterEntityCreated($productEntity, $productData);
                if (null !== $modifiedProductEntity) {
                    $productEntity = $modifiedProductEntity;
                }

                /**
                 * Проверяем, существует ли товар и обновляем/создаем
                 */
                $existingProduct = $productFinder->getExisting($productEntity);

                if (empty($existingProduct)) {
                    if ($bProdMode) {
                        $productId = $productRepository->add($productEntity);

                        $logger->info(sprintf(
                            'Добавлен новый товар %d - %s', $productId, $productData['id']
                        ));

                        $exchangeStat['created']++;
                    }
                } else {
                    $productId = $existingProduct['ID'];

                    // Если режим принудительного обновлнеия или изменился хэш
                    if ($forceUpdate || $existingProduct['PROPERTY_' . $hashPropCode . '_VALUE'] !== $productDataHash) {

                        $productRepository->update($existingProduct['ID'], $iblockId, $productEntity, $bProdMode);
                        $logger->info(sprintf(
                            'Обновлен товар %d - %s', $existingProduct['ID'], $productData['id']
                        ));

                        $exchangeStat['updated']++;

                    } else  if ($bProdMode) {
                        $changesDetector->setUpdatedFlag($productId, $iblockId);
                    }
                }

                if ($bProdMode) {
                    // Обновим/Создадим элемент каталога
                    $catalogProduct = Functions\Catalog\getCatalogProductDataFromApiProduct($productId, $productData);

                    $existingCatalogProduct = $catalogProductFinder->getExisting($productId);

                    if (count($existingCatalogProduct) == 0) {
                        // Если элемент каталога не создан
                        Functions\Catalog\addCatalogProduct($catalogProduct);

                        $logger->info(sprintf('Добавлен элемент каталога %d', $productId));
                    } else if (
                        $existingCatalogProduct['WEIGHT'] != $catalogProduct['WEIGHT']
                        || $existingCatalogProduct['WIDTH'] != $catalogProduct['WIDTH']
                        || $existingCatalogProduct['HEIGHT'] != $catalogProduct['HEIGHT']
                        || $existingCatalogProduct['LENGTH'] != $catalogProduct['LENGTH']
                    ) {
                        // Если данные изменились
                        Functions\Catalog\updateCatalogProduct($catalogProduct);

                        $logger->info(sprintf('Обновлен элемент каталога %d', $productId));
                    }
                }

                $totalProductsProcessed++;

            } catch (BadProductDataException $ex) {
                $logger->error($logger->formatException($ex, 'Формат данных из АПИ не соответствует ожидаемому'), [
                    'product_id' => $productData['id'],
                    'product_data' => $productEntity->toState(),
                    'trace' => $ex->getTraceAsString()
                ]);

                $exchangeStat['errors']++;

            } catch (ProductSaveException $ex) {
                $logger->error($logger->formatException($ex, 'Ошибка сохранения товара в БД'), [
                    'product_id' => $productData['id'],
                    'product_data' => $productEntity->toState(),
                    'trace' => $ex->getTraceAsString()
                ]);

                $exchangeStat['errors']++;

            } catch (NoMatchingSectionException $ex) {
                $logger->error($logger->formatException($ex, 'Для товара нет соответствующего раздела, пропускаем'), [
                    'product_id' => $productData['id'],
                ]);

                $exchangeStat['errors']++;

            } catch (Exception $ex) {
                $logger->critical($logger->formatException($ex), [
                    'product_id' => $productData['id'],
                    'trace' => $ex->getTraceAsString()
                ]);

                $exchangeStat['errors']++;
            }
        }

        //TODO::remove
        //break;

    } while (count($records) > 0);

    if ($bProdMode) {
        $exchangeStat['deactivated'] = $changesDetector->deactivateNonExistingProducts($iblockId);
    }

    $logger->state('Завершен обмен', $exchangeStat);

    /**
     * Вывод суммарной статистики
     */
    echo '***************** Статистика по обмену ************' . $lineEnding;

    echo 'Обработано:' . $exchangeStat['processed'] . $lineEnding;
    echo 'Добавлено новых:' . $exchangeStat['created'] . $lineEnding;
    echo 'Обновлено:' . $exchangeStat['updated'] . $lineEnding;
    echo 'Ошибок:' . $exchangeStat['errors'] . $lineEnding;
    echo 'Отключено:' . (int)$exchangeStat['deactivated'] . $lineEnding;


} catch (Exception $ex) {
    echo $ex->getMessage() . PHP_EOL;

    $logger->critical($logger->formatException($ex), [
        'trace' => $ex->getTraceAsString()
    ]);
}